{{{
  "title": "Carnes",
  "tags": ["India Pale Ale (IPA)", "Altbier", "Irish Red Ale", "Pilsen", "Brown Ale"],
  "category": "Receita Tipo",
  "video": false
}}}
Para filés, costeletas, churrascos e todo tipo de comida de sabores defumados, além das tradicionais lagers, uma dica é saborear as cervejas escuras, estilo Dunkel e, também as do estilo Red Ale. Representando essas categorias podemos citar as Erdinger Weissbier Dunkel e a Murphy's Irish Red Beer, ambas importadas. Experimente também as representantes nacionais Eisenbahn Dunkel, Therezópolis Ebenholz e Baden Baden Red Ale, elas funcionam muito bem em churrascos.