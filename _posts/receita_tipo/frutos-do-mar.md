{{{
  "title": "Frutos do Mar",
  "tags": ["Belgian Golden Strong Ale", "Weissbier"],
  "category": "Receita Tipo",
  "video": false
}}}
Se você estiver na praia onde a culinária predominante são pratos e porções a base de peixe e frutos do mar, você pode escolher alguns estilos de cervejas que são refrescantes, mas que também harmonizam com estes alimentos. Quando falamos de peixes e frutos do mar a harmonização clássica é a Weizenbier, cerveja de trigo, tradicional do sul da Alemanha, possui uma turbidez natural, aromas e sabores frutados e cítricos, bastante refrescante. Outra opção seriam as Witbier, cerveja de trigo belga, mais leve que as Weizenbier, com aromas condimentados e cítricos.