{{{
  "title": "India Pale Ale (IPA)",
  "category": "Cerveja Tipo",
  "video": "9weOfRGLhTQ"
}}}
Teve sua origem no século XVIII, quando era fabricada especialmente para os ofociais do Exército britânico residentes na índia durante o peíodo colonial, que enfrentavam muito calor e ausência de água potável. Para ajudar a suportar a longa viagem desde a Inglaterra, a bebida era produzida com bastante lúpulo, o que lhe conferia sabor fortemente frutado e maior teor alcoólico.

Os três estilos mais famosos são original English IPA, a versão americana American Ipa e o estilo Imperial IPA, que na verdade é uma inovação americana para atender a demanda de cervejas mais amargas e alcoólicas.

A English IPA foi criada aumentando-se  o teor de lúpulo e de álcool. O aroma do lúpulo é sentido imediatamente ao servir, o que é sua característica principal. Sua cor varia do âmbar-dourado ao cobre claro e apresenta pouca mais persistente espuma.

A American IPA, a versão americana, é produzida com ingredientes nativos e é levemente mais amarga que a versão inglesa, refletindo as características do lúpulo americano, que são mais perfumados, cítricos e  florais.

A Imperial IPA surgiu recentemente nos Estados Unidos para suprir a demanda por cervejas mais amargas. É uma cerveja clara, complexa, com uma espuma persistente e seu teor alcoólico pode chegar a 10%.

