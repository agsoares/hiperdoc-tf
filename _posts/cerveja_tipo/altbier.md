{{{
  "title": "Altbier",
  "category": "Cerveja Tipo",
  "video": false
}}}
A Altbier é uma lagered ale, ou seja, uma ale que passa por um processo de maturação semelhante aos das cervejas de baixa fermentação (processo em alemão chamado de lagerung). 

Existem duas divisões de Alt, a Northern German Altbier e a Düsseldorf Altbier. A maior diferença entre os dois estilos reside na lupulagem que é um pouco maior nas Alt feitas em Düssedorf.

É uma cerveja bem balanceada e com amargor um pouco mais pronunciado.

