﻿{{{
  "title": "Pilsen",
  "category": "Cerveja Tipo",
  "video": false
}}}
É um tipo de cerveja de baixa fermentação fabricada com maltes especiais e produzido na cidade de Pilsen (Plzeň), situada na região da Boêmia, na atual República Checa.

A cerveja Pilsen apresenta uma coloração clara, de tonalidade dourada brilhante, produzido pelo processo de fermentação profunda, com teor alcoólico baixo, entre 3 e 5%, possui um teor de extrato primitivo, que varia de 11 a 13,5%. As pilsener é uma cerveja de baixa fermentação (lager), e deve ter a temperatura controlada entre 10 e 12 graus durante o processo de fermentação, e deve ser bebida gelada, entre 3 e 6 graus centígrados.

Atualmente, cerca de 60% de todas as cervejas e chopps Pilsen produzidos no mundo são deste tipo. No Brasil, o consumo de cerveja Pilsen representa 98% do mercado, devido principalmente ao clima favorável, ficando o restante para as do tipo bock, light, malzbier e stout. 