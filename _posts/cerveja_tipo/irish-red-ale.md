{{{
  "title": "Irish Red Ale",
  "category": "Cerveja Tipo",
  "video": false
}}}
Aroma de malte de baixo a moderado puxado para biscoito e caramelo.

Aroma de lúpulo de baixo a nenhum, possui coloração âmbar profundo, límpida, com colarinho baixo, de cor quase branco a bege. Seu sabor e dulçor moderados de malte caramelo, ocasionalmente com um tostado amanteigado ou lembrando toffee.

Final com um leve gosto de grão torrado, que propicia uma típica secura no final. Baixo amargor com final de boca moderadamente seco. Possui corpo médio, carbonatação moderada, característica que fazem dela um cerveja de alto drink-hability.

Pode conter alguns adjuntos (milho, arroz ou açúcar), embora o uso excessivo de adjuntos possa prejudicar o caráter da cerveja. Geralmente tem um pouco de cevada torrada para conferir a cor avermelhada e o final torrado e seco. Maltes, lúpulos e leveduras do Reino Unido/Irlanda.