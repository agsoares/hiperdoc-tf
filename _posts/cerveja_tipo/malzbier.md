{{{
  "title": "Malzbier",
  "category": "Cerveja Tipo",
  "video": false
}}}
Malzbier é uma cerveja doce, com baixo teor alcoólico (geralmente entre 0 - 4%) e de cor escura. Geralmente usada como uma bebida energética.

As Malzbiers brasileiras são cervejas do tipo american pale lager na qual, após a filtração, são adicionados caramelo e xarope de açúcar, dando a coloração escura (que não vem do malte tostado) e o sabor adocicado. 

Muito famosa no Brasil, não possui muitos correspondentes fora do país. Na Alemanha, seu país de origem, não é considerada cerveja, e sim bebida energética. Enquadra-se normalmente no grupo de “outras cervejas com baixo teor alcoólico”, já que a Malzbier original não chega nem a 1% de álcool, pois, apesar de ser produzida como qualquer outra cerveja, ela quase não tem fermentação.