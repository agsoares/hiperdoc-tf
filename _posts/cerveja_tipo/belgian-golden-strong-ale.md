{{{
  "title": "Belgian Golden Strong Ale",
  "category": "Cerveja Tipo", 
  "video": false
}}}
Este é um dos mais clássicos estilos de cerveja, embora muito limitado à Bélgica. Sua cor clara sugere uma Lager leve, mas não é. São bebidas fortes, muito frutadas, com bastante lúpulo (amargas) e de alto teor alcoólico.

Bastante efervescentes, produzem uma espuma densa e cremosa, que se agarra ao copo, produzindo o chamado “Belgian Lace” à medida que o copo se esvazia.

Pelo teor alcoólico, são muitas vezes confundidas com as Tripel, e vice-versa. Entretanto, os exemplos deste estilo são bebidas mais claras e efervescentes, que provocam sensação crocante e seca, com retrogosto levemente amargo.

Os nomes usados pelas cervejarias para seus produtos deste estilo são sempre curiosos e tentam fazer alusão ao seu alto teor alcoólico. A cerveja original deste estilo é a Duvel, que significa “diabo”, produzida na cervejaria Moortgat desde o fim da Primeira Guerra Mundial.