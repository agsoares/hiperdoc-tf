{{{
  "title": "Kölsch",
  "category": "Cerveja Tipo",
  "video": false
}}}
O estilo Kölsch é originário da cidade de Colônia na Alemanha, e tradicionalmente é servido em um copo cilíndrico e comprido contendo o volume de 200ml. Por ser uma cerveja de elevada drinkability e baixo teor alcoólico, é confundida com as cervejas de baixa fermentação. 

Tradicionalmente partem de um processo de alta fermentação, porém com a maturação a frio (lager). Hoje algumas cervejarias usam leveduras capazes de atuar em ambas as faixas, assim a Kölsch está inserida na categoria das cervejas híbridas.

O termo "Kölsch" é protegido pela Kölsch Konvention e só pode ser utilizado por cerca de vinte cervejarias locais que produzem, cada uma, a sua cerveja, resultando em inúmeras variações.

