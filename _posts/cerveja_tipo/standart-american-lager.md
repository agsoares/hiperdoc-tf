{{{
  "title": "Standard American Lager",
  "category": "Cerveja Tipo",
  "video": false
}}}
Refrescante, e de um amarelo bem claro, é transparente, com espuma clara, mas pouco persistente, e aroma discreto. De todos os estilos de cerveja, este é o mais popular, e representa a maior parte das vendas no mundo.

Sua maior diferença entre à Lite America Lager é o seu teor alcoólico. Isso, associado ao acréscimo de um pouco mais de lúpulo, faz com que apresente mais corpo.

De longe o favorito do brasileiro. Perfeito para os dias quentes, tão comuns ao longo do ano. 

Tem coloração amarelo palha bem claro a amarelo médio. Espuma branca, em geral não muito estável. Bastante brilhante.

Pouco aromática no geral, apresenta raros aromas provenientes do malte (grainy ou adocicado percebidos eventualmente). Aromas de lúpulo variam de zero até leve floral ou condimentado. Baixo a médio amargor.

Áromas intensos da fermentação ou maturação são considerados defeitos nesse estilo.