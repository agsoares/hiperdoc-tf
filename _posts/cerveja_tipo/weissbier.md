{{{
  "title": "Weissbier",
  "category": "Cerveja Tipo",
  "video": "yH2vjzRfT04"
}}}
Do sul da Alemanha, é a típica cerveja do verão europeu, considerada "espumante" pelos alemães, que costumam bebê-la no desjejum. Geralmente não é filtrada, o que lhe dá uma aparência opaca. Tem um leve sabor de fruta (banana, maçã ou ameixa). É muito clara, com espuma branca e abundante e sempre efervescente.

São cerveja de trigo feitas para serem apreciadas com o fermento, o que lhes dá uma aparência turva e esbranquiçada. Assim, são conhecidas também como cervejas brancas, ou “weiss” na língua germânica. É tradicional do Sul da Alemanha, têm sabor frutado, lembrando banana, e fenólico, semelhante a cravo ou noz moscada. 

A cor varia de amarelo claro a âmbar claro, com uma bela e consistente espuma e com corpo de leve a médio.