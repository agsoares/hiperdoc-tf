{{{
  "title": "Brown Ale",
  "category": "Cerveja Tipo",
  "video": false
}}}
São cervejas com bastante sabor, forte presença de lúpulo e muito habituais em pequenas explorações ou mesmo em produções caseiras. São similares às American Pale e American Amber Ales, diferindo destas por terem uma componente mais forte de caramelo e chocolate, que servem para equilibrar o lúpulo e o final algo amargo. 

A cor varia entre um castanho-avermelhado a castanho escuro e a espuma deverá ser creme e não muito duradoura. Possui menos álcool que uma Porter e tem, em geral, um sabor complexo e bem balanceado.