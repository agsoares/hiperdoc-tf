﻿{{{
  "title": "Porter",
  "category": "Cerveja Tipo",
  "video": false
}}}
De coloração escura e elaborada com bastante malte torrado, as cervejas deste estilo são de alta fermentação e teor alcoólico que pode ser considerado alto, podendo levar tanto fermento Lager como Ale. 

Uma de suas principais características – sabores e aromas de café e malte – garantem que elas serão lembradas e marcantes. O lúpulo bem presente garante a ela o amargor exato para quem possui um paladar de gostos mais fortes. Tem um agradável sabor amargo e um elevado teor de ácido carbónico.

Atualmente, este tipo de cerveja é uma especialidade das cervejarias dos Estados Unidos e pode ter até 12% de álcool.