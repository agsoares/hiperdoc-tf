{{{
  "title": "Stout",
  "category": "Cerveja Tipo",
  "video": "Bg39IE5aFg"
}}}
É um estilo de cerveja bem escura e que muitas vezes tem um sabor torrado ou semelhante a café. Enquanto a maior parte das versões comerciais depende primariamente de cevada torrada como grão escuro, outras usam malte chocolate, malte black ou combinações dos três. O conteúdo alcoólico e a sensação "seca" (pouco açucar) de uma stout são ambas caracterizadas como leve, embora varie de país para país. A secura provém do uso da cevada torrada não maltada.

Esse estilo originou-se na Irlanda, produzido a partir de cevada torrada, que produz um malte especial (escuro) e possui um sabor amargo conferido pelo lúpulo associado ao adocicado do malte. Enquadrado como cerveja de fermentação superficial, por possuir alto teor de álcool (5 a 6,5%). Possui também elevado teor de extrato primitivo, cerca de 15%.