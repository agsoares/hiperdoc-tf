{{{
  "title": "Sopa de Legumes",
  "tags": ["Sopas"],
  "category": "Receita",
  "video": "CmW_luI1Nto"
}}}
INGREDIENTES

- 500g de batatas
- 3 cenouras grandes partidas em pedaços
- 350g de abóbora cortada em cubos
- 1 alho francês cortado em rodelas
- 1 nabo cortado ao meio
- 1 cebola cortada ao meio
- 200g de ervilhas
- 2 cenouras cortadas em cubinhos
- 170g de folhas de espinafres
- 1 cubo de caldo de galinha
- 100 ml de azeite
- Sal q.b.


MODO DE PREPARO

1. Numa panela com 4 litros de água a ferver, junte as batatas, as cenouras cortadas em pedaços, o alho francês, o nabo, a cebola, a abóbora e o cubo de caldo de galinha. Tempere com sal. Tape e deixe cozer durante 40 minutos.

2. Depois dos legumes cozidos, passe a sopa com a varinha mágica. Depois da sopa passada, adicione as ervilhas, as cenouras cortadas em cubinhos, os espinafres e o azeite. Mexa e deixe cozinhar durante 25 minutos. Depois de tudo cozido está pronta a servir.