{{{
  "title": "Carne Assada",
  "tags": ["Carnes"],
  "category": "Receita",
  "video": false
}}}
INGREDIENTES
- 1 kg e 200 g de alcatra
- 1 pedaço de bacon do tamanho da carne
- 1 linguiça de paio ou calabresa
- Alho, sal, pimenta (a gosto)
- Salsa picada (a gosto)
- 1 pitada de manjericão
- 2 colheres de vinagre

MODO DE PREPARO
1. Misture o alho, pimenta, salsa, vinagre e o manjericão
1. Depois coloque os pedaços de bacon e a linguiça dentro da carne
1. Com a mistura tempere a carne e reserve por, no mínimo, 40 minutos
1. Depois coloque a carne em uma assadeira untada com óleo (3 colheres)
1. Coloque manteiga por cima da carne e forre com papel alumínio
1. Leve ao forno e depois de 1 hora retire o papel
1. Marinar a carne até dourar
1. Se achar necessário, dissolva um caldo de carne e marine com isso até dourar
1. Se preferir cozinhe uma batatas calabresas e na hora em que a carne estiver dourando adicione as batatas
1. Elas irão dourar junto com a carne