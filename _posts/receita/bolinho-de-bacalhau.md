{{{
  "title": "Bacalhau ao Forno",
  "tags": ["Frutos do Mar"],
  "category": "Receita",
  "video": "YcuGplnuPxo"
}}}
INGREDIENTES
- 1 kg de bacalhau
- 1/2 kg de batata
- 1/2 kg de cenoura
- 2 cebolas grandes
- 2 tomates (vermelho e/ou amarelo)
- 1 pimentão
- 1 colher de sopa de extrato de tomate
- Leite de coco
- Alho, sal, coentro e azeite de oliva a gosto
- Azeitonas

Molho: 
- 2 copos de leite
- 1 colher de sopa de amido de milho
- 1 colher de sopa de manteiga
- 1 ovo
- 1/2 xícara de creme de leite
- Noz-moscada, pimenta-do-reino e sal

MODO DE PREPARO
1. Dessalgue o bacalhau por 12 horas, em geladeira, trocando a água
1. Corte em lascas o bacalhau, retire a pele
1. Em uma assadeira coloque azeite de oliva no fundo, faça uma cama com cebola em rodelas, coloque por cima batatas cozidas em rodelas
1. Coloque por cima o bacalhau em lascas
1. Coloque outra camada de cebola, um pouco de alho em lascas, adicione pimentão cortado em palitos
1. Coloque cenoura picadinha, algumas azeitonas (pode ser sem caroço), o tomate sem sementes e cheiro verde picado
1. Acrescente o molho branco por cima, um pouco mais de cheiro verde, 1 colherada de extrato de tomate e regue leite de coco
1. Finalize com um pouco de azeite de oliva
1. Leve ao forno por 30 minutos à 180°C para gratinar
1. Sirva com arroz branco

Molho: 
1. Bata no liquidificador o leite, o amido de milho e a manteiga derretida
1. Leve ao fogo e mexa bastante até engrossar a mistura
1. Finalmente, junte o creme de leite, a noz-moscada, a pimenta-do-reino, o sal e o ovo batido