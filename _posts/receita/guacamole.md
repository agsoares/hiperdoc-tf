{{{
  "title": "Guacamole",
  "tags": ["Saladas"],
  "category": "Receita",
  "video": "Ynm7Csf-cKc"
}}}
INGREDIENTES

- 3 abacates pequenos sem casca e sem caroço
- Suco de 1 limão
- 1 colher (chá) de sal
- 1/2 cebola picada
- 1 punhado de coentro fresco picado
- 2 tomates grandes, picados
- 1 dente de alho, amassado
- 1 pitada de pimenta caiena (opcional)


MODO DE PREPARO

Em uma vasilha média, amasse bem os abacates e junte o sal e o suco de limão. Acrescente a cebola, o coentro, o tomate, o alho e a pimenta. Misture tudo e leve à geladeira por 1 hora para apurar o sabor ou sirva imediatamente.