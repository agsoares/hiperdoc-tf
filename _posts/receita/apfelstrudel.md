{{{
  "title": "Apfelstrudel",
  "tags": ["Doces"],
  "category": "Receita",
  "video": "cz15xW9UD40"
}}}
INGREDIENTES

- 1 pacote de massa folhada congelada
- 1 colher de sopa de uvas passas
- 2 colheres de sopa de rum
- 4 colheres de sopa de manteiga
- 1/2 xícara de farinha de rosca
- 4 maçãs
- 7 colheres de sopa de açúcar
- 2 colheres de sopa de nozes picadas
- 200g de nata
- Essência de baunilha
- 1 gema de ovo

MODO DE PREPARO

1. Deixe as passas hidratando em duas colheres de sopa de rum por 20 minutos.
2. Em uma frigideira, derreta 3 colheres de manteiga, adicione a farinha de rosca e mexa até dourar.
3. Deixe essa farofa esfriando e cortar as maçãs bem lavadas em fatias finas.

Em uma tigela, misturar as maçãs, a farofa, as passas com o rum, 3 colheres de açúcar e as nozes picadas.
Enfarinhar a bancada e abrir com o rolo a massa folhada, até ficar mais fininha.
Passar a massa para cima de um pano de prato bem limpo e enfarinhado.

Dispor o recheio em um dos lados, pincelar todas as bordar com 1 colher de manteiga derretida e enrolar com a ajuda do pano.
Colocar o strudel em uma forma e pincelar toda superfícies com a gema batida.

Levar ao forno médio, preaquecido, até ficar dourado e crocante, por mais ou menos meia hora. Para o chantilly, basta bater a nata com 4 colheres de açúcar e a essência de baunilha.