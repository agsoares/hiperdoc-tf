{{{
  "title": "Caesar Salad",
  "tags": ["Saladas"],
  "category": "Receita",
  "video": "tl9WeiIs_7M"
}}}
INGREDIENTES

Para o molho:

- 2 colheres de sopa de maionese
- 2 dentes de alho
- 1 colher de sopa de mostarda dijon
- 1/4 de xícara de suco de limão siciliano
- 1 xícara de azeite de oliva
- 1/2 xícara de queijo parmesão ralado fino 

Para os croutons:

- 2 fatias de pão de forma (ou outro se preferir) cortados em cubos
- 1 colher de sopa de azeite de oliva 
- 1 colher de sopa de orégano


MODO DE PREPARO

Em uma vasilha média, amasse bem os abacates e junte o sal e o suco de limão. Acrescente a cebola, o coentro, o tomate, o alho e a pimenta. Misture tudo e leve à geladeira por 1 hora para apurar o sabor ou sirva imediatamente.