{{{
  "title": "Hamburger",
  "tags": ["Carnes"],
  "category": "Receita",
  "video": false
}}}
INGREDIENTES
- 1/2 kg de carne bovina
- 1 envelope de creme de cebola
- 1 ovo inteiro
- Sal e pimenta a gosto
- Óleo para fritar
- Alface
- Tomate
- Cebola
- Maionese
- Pão de hamburger

MODO DE PREPARO
1. Em um recipiente, coloque a carne moída, o creme de cebola, o ovo levemente batido, o sal e a pimenta
1. Misture para se agregarem
1. A seguir, coloque uma porção da mistura obtida em um saco plástico e passe o rolo
1. Retire o saco e modele o hamburger com cortador próprio ou um cortador redondo
1. Frite em óleo quente
1. Sirva com pão de hamburger, maionese, alface, tomate e cebola