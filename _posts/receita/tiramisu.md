{{{
  "title": "Tiramisu",
  "tags": ["Doces"],
  "category": "Receita",
  "video": "rbncfh5S5y0"
}}}
INGREDIENTES

- 6 ovos (claras e gemas separadas)
- 3 colheres (sopa) de açúcar
- 500 g de queijo mascarpone
- 50 ml de conhaque
- 350 ml de café
- 4 colheres (sopa) de chocolate em pó
- 30 bolachas champagne

MODO DE PREPARO

1. Bata as gemas com o açúcar até formar um creme claro. Adicione o queijo, metade do conhaque e continue batendo.
2. Numa vasilha separada, bata as claras em neve. Incorpore-as delicadamente ao creme de queijo, misturando até que fique homogêneo. Reserve.
3. Despeje o café numa tigela rasa. Adicione a outra metade do conhaque e 1 colher de chá do chocolate em pó. Mergulhe as bolachas no café e distribua-as no fundo de um refratário.
4. Cubra as bolachas com metade do creme e polvilhe chocolate em pó por cima. Repita o procedimento com mais uma camada de bolachas, a outra metade do creme e chocolate em pó.
5. Leve à geladeira por 4 horas antes de servir.