{{{
  "title": "Bobó de Camarão",
  "tags": ["Frutos do Mar"],
  "category": "Receita",
  "video": "S-pbeHV8qPA"
}}}
INGREDIENTES
- 1 Kg de camarões frescos
- sal
- 3 dentes de alho picados e amassados
- suco de 1 limão
- pimenta do reino
- 1 Kg de mandioca 
- 1 cebola cortada em rodelas e 2 cebolas raladas
- 1 folha de louro
- 6 colheres de sopa de azeite de oliva
- 2 vidros de leite de coco
- 1 maço de cheiro verde picado
- 2 latas de molho pronto de tomate
- 2 pimentões verdes bem picadinhos
- 2 colheres de sopa de azeite de dendê

MODO DE PREPARO
1. Lave os camarões e tempere com sal, alho, pimenta e limão
2. Deixe marinar
3. Pegue uma panela com água e cozinhe a mandioca em pedacinhos com louro e cebola em rodelas
4. Quando estiver mole acrescente um vidro de leite de coco, deixe esfriar um pouco e bata no liquidificador
5. Esquente o azeite de oliva, junte a cebola ralada e deixe dourar
6. Acrescente os camarões e frite
7. Adicione as 2 latas de de tomate, o cheiro verde, o pimentão e deixe cozinhar por alguns minutos
8. Junte na mesma panela a mandioca batida no liquidificador, outro vidro de leite de coco e o azeite de dendê, deixe levantar fervura e está pronto