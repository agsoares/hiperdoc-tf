{{{
  "title": "Cheesecake",
  "tags": ["Doces"],
  "category": "Receita",
  "video": "vTjDQXS7s1g"
}}}
Para a massa
INGREDIENTES

- 200 g de bolacha maisena
- 100g de manteiga sem sal

MODO DE PREPARO

Use o liquidificador para transformar as bolachas numa farinha fina. Depois, pegue essa farinha e misture com a manteiga até tudo virar uma massa. Forre o fundo da forma.

Para o recheio

INGREDIENTES

- 1 lata de leite condensado
- 500 g de ricota fresca
- 4 ovos
- 200 g de iogurte natural
- 5 ml de baunilha
- 2 limões

MODO DE PREPARO

Bata todos os ingredientes no liquidificador e despeje sobre a massa do biscoito. Leve ao forno em banho-maria, por 40 minutos.

Para a cobertura

INGREDIENTES

- 100 g de framboesas
- 100 g de amoras
- 100 g de morangos
- 200 g de açúcar refinado
- 5 ml de suco de limão

MODO DE PREPARO

Em uma panela, junte as frutas, o açúcar e o suco do limão. Não pare de mexer até virar uma calda brilhante.

Quando tudo estiver frio, retire o cheesecake da forma e despeje a calda por cima. 