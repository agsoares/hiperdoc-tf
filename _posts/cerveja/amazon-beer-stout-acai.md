﻿{{{
  "title": "Amazon Beer Stout Açaí",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Cerveja fiel ao estilo Dry Stout. Apresenta aroma e sabor de café, toffe, chocolate e malte torrado. Fortificada pela energia do açaí, o mais emblemático fruto da região amazônica. Escura e encorpada, apresenta espuma de boa formação e persistência. Uma autêntica força da natureza.

Cerveja encorpada (corpo médio/alto), com predominância do amargor dos maltes torrados, café e chocolate amargo. 

Possui coloração preta, com espuma marrom volumosa e de média duração, criando uma aparência muito convidativa.