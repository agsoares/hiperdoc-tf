﻿{{{
  "title": "Southern Tier 2XStout",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Possui coloração negra, opaca, com espuma marrom de média formação e duração.

Notas de lúpulo herbáceo, malte torrado, chocolate e leite. Leve aroma de chocolate e grãos de café torrados. Sabor de lúpulos terrosos, levedura picante, caramelos, leite, chocolate amargo, alcaçuz e terminando com um café com cinzas. Amargor acentuado proveniente da torrefação e da lupulagem.

Cerveja encorpada e de amargor acentuado, chegando a lembrar o de uma Russian Imperial Stout.