{{{
  "title": "Bamberg Alt",
  "tags": ["Altbier"],
  "category": "Cerveja",
  "video": false
}}}
Criada na cidade alemã de Dusseldorf é uma cerveja marrom avermelhada, de alta fermentação, possuindo um aroma característico que se origina desse processo, ao bebê-la percebe-se um grande complexidade de sabores. Com uma lupulagem acentuada, é ideal para ser consumida no outono ou inverno junto a pratos mais temperados e fortes, como salsichas alemãs, embutidos, carnes vermelhas, cordeiros, entre outros. Encontra-se disponível apenas nos meses de maio, junho, julho e agosto. Temperatura ideal de servir 6 à 8°C.
