{{{
  "title": "Eisenbahn Golden Strong Ale",
  "tags": ["Belgian Golden Strong Ale"],
  "category": "Cerveja",
  "video": false
}}}
Dourada levemente turva, com espuma branca de média altura e média duração. 

Aroma perfumado, mostra uma boa combinação do álcool, malte e lúpulo, deixando-a complexa, com um toque esmaltado. 

Na boca é doce, porém equilibrada, portanto na medida certa. Cerveja muito harmoniosa, principalmente pelo alto volume de álcool que fica muito bem inserido. Breja difícil de descrever, não vai pro lado caramelo mas tem um dulçor bem característico, com uma pegada frutada e condimentada, reforçados pela pegada mais alcoólica, não no tato mas sim no sabor. Alias, o álcool nada se sente na sensação de boca. O amargor aparece mais no final do gole, chega a aparecer um pouco na garganta mas logo desaparece.

Retrogosto doce, de média duração, sem sinais do lúpulo.
Uma ótima cerveja, com belíssimo custo/benefício.