﻿{{{
  "title": "Troubadour Obscura",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Coloração avermelhada, com creme marrom denso e persistente. Café. Frutado. Malte torrado. Carbonatação média. Álcool e amargor estão presentes, porém leves. Espuma bege alta, densa e de longuíssima duração. 

Possui aroma intenso de café, cravo e chocolate. Paladar levemente frutado. Creme marrom, com alta consistência, e corpo aveludado. Leve carbonatação, corpo cremoso com leve licorosidade, aquecimento alcoólico leve.
