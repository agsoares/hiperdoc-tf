﻿{{{
  "title": "Murphy's Draught Irish Stout",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Como uma legítima cerveja irlandesa, a Murphy’s Irish Stout tem sabor marcante e apresenta o tradicional sistema Draughtflow, que cria uma espuma incrivelmente cremosa que é marca registrada deste tipo de bebida. Possui mais de 150 anos de tradição, com qualidade reconhecida e premiada internacionalmente, além de utilizar malte torrado e chocolate na sua produção.

O efeito provocado pelo nitrogênio dá um visual fantástico a ela. No sabor e no aroma predominam o café e a torrefação. A carbonatação é baixa, e o final é levemente seco e amargo, deixando traços de café e torrefação na boca.