﻿{{{
  "title": "Nils Oscar Hop Yard IPA",
  "tags": ["India Pale Ale (IPA)"],
  "category": "Cerveja",
  "video": false
}}}
Aparência é de coloração cobre levemente turvo, com espuma bege alta, densa e de longa duração. 

Aroma intenso que desprende fácil da cerveja mesmo ainda bem gelada, frutado do lúpulo, lembrando frutas amarelas, especialmente um abacaxi bem doce. O lúpulo predomina no aroma, mas nota-se também o malte, que mostra ainda mais intensidade no sabor.

De corpo médio, tem um toque sutil, com carbonatação média pra baixa e amargor acentuado.
