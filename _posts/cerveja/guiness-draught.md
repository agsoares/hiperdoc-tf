﻿{{{
  "title": "Guinness Draught",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
A Guinness Draught é uma cerveja tipo Stout, de alta fermentação, feita com percentual de malte torrado, que lhe confere sua marcante cor rubi-vermelho escura e um paladar tostado. O lúpulo utilizado confere à Guinness um distinto sabor, com excelente balanço entre o amargor e a doçura. Sua espuma densa e cremosa torna a Guinness uma cerveja extremamente saborosa e mais robusta que qualquer cerveja super-premium. 

A cervejaria St. James Gate está localizada na cidade de Dublin na Irlanda desde 1759. A Guinness é dita como a cerveja Stout mais consumida no mundo.