﻿{{{
  "title": "Dragon's Milk",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Essa Imperial Stout é maturada em barris de carvalho da New Holland Brewing.

O "Leite de Dragão" possui coloração rubi profunda, quase negra. O creme é medianamente denso e persistente, porém, dentro do estilo proposto.

No complexo aroma, senti bourbon, baunilha, madeira, chocolate, caramelo. O sabor traz todas as percepções do aroma, agraciado com a presença quente do álcool.

O corpo é licoroso e a carbonatação compreensivelmente baixa. O glorioso final é achocolatado, vínico e algo tostado.