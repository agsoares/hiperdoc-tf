{{{
  "title": "Delirium Tremens",
  "tags": ["Belgian Golden Strong Ale"],
  "category": "Cerveja",
  "video": "QJX6UvZBV9E"
}}}
A famosa cerveja do Elefantinho Rosa. Tem uma belíssima apresentação a começar pela garrafa e rótulo. Ao ser colocada no copo, também mostra sua presença, com uma cor dourada, translúcida, boa carbonatação aparente e uma espuma alta, farta e duradoura, levemente bege. 

No aroma e no sabor, mostra-se frutada (principalmente cítrica, mas com toques de abacaxi e damasco) e floral, com um bom toque de malte caramelo. 

Encorpada e complexa, não decepciona a classe de boas cervejas belgas. No páreo direto, prefiro ela que a Duvel. Com certeza uma das referências no mundo das cervejas, deve estar na lista de todos que buscam provar as melhores cervejas do mundo.