﻿{{{
  "title": "Colorado Kölsch",
  "tags": ["Kölsch"],
  "category": "Cerveja",
  "video": false
}}}
Amarela bem clara, creme de pouca formação e duração. Aroma com frutado bem pronunciado, paladar com leve amargor e ótima drinkability.

Cerveja altamente harmonizada, com o herbal do lúpulo combinando com a alta carbonatação, que combina com o leve dulçor do malte, que combina com um discreto frutado. Pouca espuma.