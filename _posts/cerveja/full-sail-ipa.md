﻿{{{
  "title": "Full Sail IPA",
  "tags": ["India Pale Ale (IPA)"],
  "category": "Cerveja",
  "video": false
}}}
Na aparência, coloração ferruginosa translúcida e intensa. O creme é denso, consistente e persistente.

No aroma, o lúpulo Cascade toma todos os lugares, deixando pouco espaço para os maltes, os quais também aparecem, mas delicadamente.

Na boca, as sensações herbáceas lupuladas têm mais intensidade ainda. O malte levemente tostado comparece melhor, balanceando o conjunto.

O final é longo, herbáceo e com o dulçor do malte tostado.