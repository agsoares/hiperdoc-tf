﻿{{{
  "title": "Bodebrown / Stone Cacau IPA",
  "tags": ["India Pale Ale (IPA)"],
  "category": "Cerveja",
  "video": false
}}}
Cerveja American IPA com cacau feita em colaboração com a cervejaria americana Stone Brewing (Califórnia) durante a visita do CEO & fundador Greg Koch à Bodebrown para participar do primeiro Bodebrown Beer Ranch. 

É uma cerveja que não foi feita para agradar muitas pessoas, apenas aqueles que sabem reconhecer o valor e o prazer de degustar uma cerveja com aromas e sabores fortes. 

Essa Cerveja possui aromas que lembram frutas cítricas provenientes das adições de lúpulos cítricos norte-americanos combinado com os aromas que remetem ao chocolate provenientes das adições de cacau em formato "nibs" durante a fervura e maturação. 

No sabor, encontramos um balanço entre o maltado com caramelo, além do médio amargor presente através dos lúpulos.
