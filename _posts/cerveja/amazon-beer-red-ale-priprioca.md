{{{
  "title": "Amazon Beer Stout Açaí",
  "tags": ["Irish Red Ale"],
  "category": "Cerveja",
  "video": false
}}}
Cerveja da Escola Inglesa com o terroir da Amazônia. Maturada com priprioca, raiz típica da região, que surpreende por seu misterioso aroma amadeirado. Alta drinkability em notas herbais e frutadas, com maltes e lúpulos em perfeito equilíbrio. Criada para paladares exigentes.