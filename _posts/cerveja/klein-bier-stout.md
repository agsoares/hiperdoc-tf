﻿{{{
  "title": "Klein Bier Stout",
  "tags": ["Stout"],
  "category": "Cerveja",
  "video": false
}}}
Cerveja de alta fermentação com origem na Irlanda. Ao contrário das escuras mais conhecidas no Brasil, que são adocicadas e escurecidas com caramelo, a Stout da Klein é feita com maltes torrados importados que conferem a cerveja um sabor de café e chocolate. 

Outro diferencial é a utilização de gás nitrogênio em sua composição resultando em um verdadeiro creme delicioso.

Tem coloração escura e turva. No aroma e no sabor, café extremo. Final amargo, com predominância do malte torrado, toques de chocolate e longo dulçor.
