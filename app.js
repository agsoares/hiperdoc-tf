(function startServer(){
  var
    express = require('express'),
    app = express(),
    Poet = require('poet'),
    _ = require('lodash');



  var poet = Poet(app, {
    posts: './_posts/',
    postsPerPage: 5,
    metaFormat: 'json'
  });

  var convertTagToTitle = function (tag) {
    return poet.helpers.tagUrl(tag.replace(/ /g,'-').replace(/[^a-zA-Z0-9-]/g,'')).replace('/tag/','').toLowerCase();
  };

  poet.addRoute('/tipos-cerveja', function (req, res, next) {
    var list = poet.helpers.postsWithCategory("Cerveja Tipo");
    if (list.length > 0) {
      res.render('_template/list.ejs',
                { base_url:  'http://' +  req.headers.host,
                  title: 'Tipos de Cerveja',
                  list: list,
                  list_link: '/tipo-cerveja/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/tipo-cerveja/:post', function (req, res, next) {
    var post = poet.helpers.getPost(req.params.post);
    var list = [];
    if (post) {
      list = _.intersection(poet.helpers.postsWithTag(post.title), poet.helpers.postsWithCategory("Cerveja"));
      res.render('_template/content.ejs',
                { base_url:  'http://' +  req.headers.host,
                  title: post.title,
                  sub_title: 'Cervejas do Tipo',
                  video: post.video,
                  content: post.content,
                  list: list,
                  list_link: '/cerveja/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/cerveja/:post', function (req, res, next) {
    var post = poet.helpers.getPost(req.params.post);
    var list = [];
    if (post) {
      list = _.intersection(poet.helpers.postsWithTag(post.tags[0]), poet.helpers.postsWithCategory("Receita Tipo"));
      res.render('_template/content.ejs',
                { base_url: 'http://' + req.headers.host,
                  title: post.title,
                  sub_title: 'Comidas que Harmonizam',
                  video: post.video,
                  content: post.content,
                  list: list,
                  list_link: '/comida/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/comidas', function (req, res, next) {
    var list = poet.helpers.postsWithCategory("Receita Tipo");
    if (list.length > 0) {
      res.render('_template/list.ejs',
                { base_url:  'http://' +  req.headers.host,
                  title: 'Comidas',
                  list: list,
                  list_link: '/comida/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/comida/:post', function (req, res, next) {
    var post = poet.helpers.getPost(req.params.post);
    var list = [];
    if (post) {
      list = poet.helpers.postsWithTag(post.title);
      res.render('_template/content.ejs',
                { base_url:  'http://' +  req.headers.host,
                  title: post.title,
                  sub_title: 'Receitas',
                  video: post.video,
                  content: post.content,
                  list: list,
                  list_link: '/receita/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/receita/:post', function (req, res, next) {
    var post = poet.helpers.getPost(req.params.post);
    var list = [];
    if (post) {
      var food = poet.helpers.getPost(convertTagToTitle(post.tags[0]));
      if (food) {
        _(food.tags).forEach(function (tag) {
          var beer = poet.helpers.getPost(convertTagToTitle(tag));
          if (beer) {
            list.push(beer);
          }
        });
      }

      res.render('_template/content.ejs',
                { base_url: 'http://' + req.headers.host,
                  title: post.title,
                  sub_title: 'Cervejas que Harmonizam',
                  video: post.video,
                  content: post.content,
                  list: list,
                  list_link: '/tipo-cerveja/'
                });
    } else {
      res.send('404');
    }
  });

  poet.addRoute('/sobre', function (req, res, next) {
      res.render('_template/about.ejs',
                { base_url: 'http://' + req.headers.host });
  });

  poet.addRoute('/', function (req, res, next) {
      res.render('_template/index.ejs',
                { base_url: 'http://' + req.headers.host });
  });


  poet.init().then(function () {
  });


  /* set up the rest of the express app */
  app.use('/assets', express.static(__dirname + '/assets'));
  app.listen(3000);
})();
