var tabs = [];
var selected_tab = 0;
var total_tabs = 0;

function refresh() {
  tabs.each(function(){
    if ($(this).attr('tab_id') == selected_tab) {
      $(this).show();
    } else {
      $(this).hide(); 
    }
  });
  if(selected_tab == 0) {
    $('.tabber .prev').hide();
  } else {
    $('.tabber .prev').show();
  }
  
  if(selected_tab == total_tabs-1) {
    $('.tabber .next').hide();
  } else {
    $('.tabber .next').show();
  }
  
}

function next_tab() { 
  if (selected_tab < total_tabs-1) {
   selected_tab++;
   refresh();    
  }
}

function prev_tab() { 
  if (selected_tab > 0) {
   selected_tab--;
   refresh();    
  }
}

$(document).ready(function () {
  tabs = $('.tabber .tab');
  total_tabs = tabs.length;
  refresh();
});




