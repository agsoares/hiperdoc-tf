var count = 0;
var played = false;

$(document).ready(function () {
  setInterval(function() {
    if(!played) {
      count++
      if (count > 10) {
        $('video')[0].play();
        played = true;
      }
    }
  }, 1000);
});